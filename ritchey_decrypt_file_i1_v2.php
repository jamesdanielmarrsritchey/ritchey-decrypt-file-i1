<?php
#Name:Ritchey Decrypt File i1 v2
#Description:Decrypt a file from Ritchey Encryption i1 v1. Returns "TRUE" on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Minimal protection is provided so it is recommended that you encrypt multiple times.
#Arguments:'source' (required) is a file to decrypt. 'destination' (required) is the path, and name of where the decrypted file will be saved.  'password' (required) is a string that is used as a password to encrypt/decrypt the data. 'loops' (optional) is the number of times to decrypt the encrypted data. Default value is '1'. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):source:file:required,destination:file:required,password:string:required,loops:integer:optional,display_errors:bool:optional
#Content:
if (function_exists('ritchey_decrypt_file_i1_v2') === FALSE){
function ritchey_decrypt_file_i1_v2($source, $destination, $password, $loops = NULL, $display_errors = NULL){
	$errors = array();
	if (@is_file($source) === FALSE) {
		$errors[] = 'source';
	}
	if (@is_file($destination) === TRUE) {
		$errors[] = 'destination';
	}
	if (@isset($password) === FALSE){
		$errors[] = 'password';
	}
	if ($loops === NULL){
		$loops = 1;
	} else if (@is_int($loops) === TRUE){
		#Do Nothing
	} else {
		$errors[] = "loops";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Read encrypted file 1 byte at a time
		$handle = @fopen($source, 'r');
		if ($handle === FALSE) {
			$errors[] = 'handle';
			goto result;
		}
		$i = 0;
		$decrypted_bytes = array();
		while (@feof($handle) === FALSE) {
			$i++;
			$encrytped_file_byte = @fread($handle, 1);
			###Convert encrypted file byte to decimal representation [It will be an array, so convert to a string]
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/ritchey_data_to_decimal_representation_i1_v2/ritchey_data_to_decimal_representation_i1_v2.php';
			$encrytped_file_byte = ritchey_data_to_decimal_representation_i1_v2($encrytped_file_byte, TRUE);
			if (@count($encrytped_file_byte) !== 1){
				$errors[] = 'encrypted_file_byte_decimal';
				goto result;
			} else {
				$encrytped_file_byte = $encrytped_file_byte[0];
			}
			###Read corresponding 1 byte from password (treat as infinit loop!)
			$password_array = @str_split($password, 1);
			$password_ii = 0;
			$password_handle = 0;
			while($password_ii < $i) {
				if(@isset($password_array[$password_handle]) === TRUE) {
					$password_byte = $password_array[$password_handle];
				} else {
					$password_handle = 0;
					$password_byte = $password_array[$password_handle];
				}
				$password_ii++;
				$password_handle++;
			}
			###Convert password byte to decimal representation [It will be an array, so convert to a string]
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/ritchey_data_to_decimal_representation_i1_v2/ritchey_data_to_decimal_representation_i1_v2.php';
			$password_byte = ritchey_data_to_decimal_representation_i1_v2($password_byte, TRUE);
			if (@count($password_byte) !== 1){
				$errors[] = 'password_byte_decimal';
				goto result;
			} else {
				$password_byte = $password_byte[0];
			}
			###Decrement (treat as infinite loop!) the file byte by the password byte (eg: 255 - 1 = 254, 0 - 1 = 255)
			$password_byte = @intval($password_byte);
			$encrytped_file_byte = @intval($encrytped_file_byte);
			while($password_byte > 0) {
				if ($encrytped_file_byte === 0){
					$encrytped_file_byte = 255;
				} else {
					$encrytped_file_byte--;
				}
				$password_byte--;
			}
			$encrytped_file_byte = @strval($encrytped_file_byte);
			###Convert from decimal representation back to binary data [It will be a string so convert to an array]
			$encrytped_file_byte = @array($encrytped_file_byte);
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/ritchey_decimal_representation_to_data_i1_v2/ritchey_decimal_representation_to_data_i1_v2.php';
			$encrytped_file_byte = ritchey_decimal_representation_to_data_i1_v2($encrytped_file_byte, FALSE);
			###Add decrypted data to array
			//Writing data directly to the file won't work, because PHP has long standing issues with writing data too fast resulting in data not being written. Instead save it all to an array, and write it when done. This requires more memory, but at least it works.
			$decrypted_bytes[] = $encrytped_file_byte;
		}
		fclose($handle);
		//The EOF byte is getting processed too, but it doesn't need to be, and messes up the decryption. Remove it.
		$delete = @array_pop($decrypted_bytes);
		###Reprocess until $loops is 0
		$loops--;
		if ($loops > 0){
		while ($loops > 0){
			$i = 0;
			$loops--;
			foreach ($decrypted_bytes as &$encrytped_file_byte){
				$i++;
				$location = realpath(dirname(__FILE__));
				require_once $location . '/dependencies/ritchey_data_to_decimal_representation_i1_v2/ritchey_data_to_decimal_representation_i1_v2.php';
				$encrytped_file_byte = ritchey_data_to_decimal_representation_i1_v2($encrytped_file_byte, TRUE);
				if (@count($encrytped_file_byte) !== 1){
					$errors[] = 'encrypted_file_byte_decimal';
					goto result;
				} else {
					$encrytped_file_byte = $encrytped_file_byte[0];
				}
				###Read corresponding 1 byte from password (treat as infinit loop!)
				$password_array = @str_split($password, 1);
				$password_ii = 0;
				$password_handle = 0;
				while($password_ii < $i) {
					if(@isset($password_array[$password_handle]) === TRUE) {
						$password_byte = $password_array[$password_handle];
					} else {
						$password_handle = 0;
						$password_byte = $password_array[$password_handle];
					}
					$password_ii++;
					$password_handle++;
				}
				###Convert password byte to decimal representation [It will be an array, so convert to a string]
				$location = realpath(dirname(__FILE__));
				require_once $location . '/dependencies/ritchey_data_to_decimal_representation_i1_v2/ritchey_data_to_decimal_representation_i1_v2.php';
				$password_byte = ritchey_data_to_decimal_representation_i1_v2($password_byte, TRUE);
				if (@count($password_byte) !== 1){
					$errors[] = 'password_byte_decimal';
					goto result;
				} else {
					$password_byte = $password_byte[0];
				}
				###Decrement (treat as infinite loop!) the file byte by the password byte (eg: 255 - 1 = 254, 0 - 1 = 255)
				$password_byte = @intval($password_byte);
				$encrytped_file_byte = @intval($encrytped_file_byte);
				while($password_byte > 0) {
					if ($encrytped_file_byte === 0){
						$encrytped_file_byte = 255;
					} else {
						$encrytped_file_byte--;
					}
					$password_byte--;
				}
				$encrytped_file_byte = @strval($encrytped_file_byte);
				###Convert from decimal representation back to binary data [It will be a string so convert to an array]
				$encrytped_file_byte = @array($encrytped_file_byte);
				$location = realpath(dirname(__FILE__));
				require_once $location . '/dependencies/ritchey_decimal_representation_to_data_i1_v2/ritchey_decimal_representation_to_data_i1_v2.php';
				$encrytped_file_byte = ritchey_decimal_representation_to_data_i1_v2($encrytped_file_byte, FALSE);
			}
		}
		}
		@implode($decrypted_bytes);
		@file_put_contents($destination, $decrypted_bytes, FILE_APPEND | LOCK_EX);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_decrypt_file_i1_v2_format_error') === FALSE){
				function ritchey_decrypt_file_i1_v2_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_decrypt_file_i1_v2_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return TRUE;
	} else {
		return FALSE;
	}
}
}
?>